const colors = require("tailwindcss/colors");

module.exports = {
  purge: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: colors,
    fill: {
      ...colors,
      current: "currentColor",
    },
    stroke: {
      ...colors,
      current: "currentColor",
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
