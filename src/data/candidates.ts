import { loadCounts } from "./counts";
import { loadResults } from "./results";

export type CandidateData = {
  votes: number;
  id: string;
  rank: number;
  givenName: string | null;
  namePrefix: string | null;
  familyName: string | null;
  gender: "male" | "female" | null;
};

export type PartyData = {
  candidates: CandidateData[];
  seats: number;
};
export type Parties = Record<string, PartyData>;

export async function loadPartyData(): Promise<Parties> {
  const results = await loadResults();
  const counts = await loadCounts();

  const partyData: Parties = {};
  Object.entries(counts).forEach(([partyId, partyCounts]) => {
    const candidates: CandidateData[] = Object.entries(partyCounts).map(([candidateId, candidateCount], rank) => {
      const candidateResults = results[partyId]?.find(candidate => candidate.code === candidateId);
      return {
        votes: candidateCount,
        id: candidateId,
        rank: rank,
        givenName: candidateResults?.givenName ?? null,
        namePrefix: candidateResults?.namePrefix ?? null,
        familyName: candidateResults?.familyName ?? null,
        gender: candidateResults?.gender ?? null,
      };
    });
    partyData[partyId] = {
      candidates: candidates,
      seats: results[partyId]?.filter(candidate => candidate.elected).length ?? 0,
    };
  });

  return partyData;
}

