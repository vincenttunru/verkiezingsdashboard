import { readFile } from "fs/promises";
import { resolve } from "path";
import { parseStringPromise } from "xml2js";
import { ElectionIdentifier } from "./results";

type CountData = {
  EML: {
    Count: [{
      Election: [{
        ElectionIdentifier: [ElectionIdentifier];
        Contests: [{
          Contest: [{
            ContestIdentifier: [{ "$": [unknown] }];
            TotalVotes: [VoteCount];
            ReportingUnitVotes: Array<VoteCount>;
          }];
        }];
      }];
    }];
  };
};
type VoteCount = {
  ReportingUnitIdentifier: [unknown];
  Selection: Array<AffiliationVotes | CandidateVotes>;
  Cast: [unknown];
  TotalCounted: [unknown];
  RejectedVotes: [unknown];
  UncountedVotes: [unknown];
};
type AffiliationVotes = {
  AffiliationIdentifier: [{ "$": [ { Id: string; } ]; RegisteredName: [string] }]; ValidVotes: [string];
};
type CandidateVotes = {
  Candidate: [{ CandidateIdentifier: [ { "$": { ShortCode: string } } ] }];
  ValidVotes: [string];
};
export function isCandidateVotes(value: AffiliationVotes | CandidateVotes): value is CandidateVotes {
  return Array.isArray((value as CandidateVotes).Candidate);
}

type CandidateCounts = Record<string, Record<string, number>>;
export function countsToCandidateCounts(counts: CountData): CandidateCounts {
  let currentParty = "unknown";
  const candidateCounts: CandidateCounts = {};
  for(const listing of counts.EML.Count[0].Election[0].Contests[0].Contest[0].TotalVotes[0].Selection) {
    if (!isCandidateVotes(listing)) {
      currentParty = listing.AffiliationIdentifier[0].RegisteredName[0];
    } else {
      candidateCounts[currentParty] ??= {};
      candidateCounts[currentParty][listing.Candidate[0].CandidateIdentifier[0].$.ShortCode] = Number.parseInt(listing.ValidVotes[0]);
    }
  }
  return candidateCounts;
}

export async function loadCountData(sourcePath = resolve(process.cwd(), "data/tk2021/Totaaltelling_TK2021.eml.xml")): Promise<CountData> {
  const countsSource = await readFile(sourcePath, "utf8");
  const counts: CountData = await parseStringPromise(countsSource);
  return counts;
}

export async function loadCounts(sourcePath = resolve(process.cwd(), "data/tk2021/Totaaltelling_TK2021.eml.xml")): Promise<CandidateCounts> {
  const countData = await loadCountData(sourcePath);
  const counts = countsToCandidateCounts(countData);
  return counts;
}
