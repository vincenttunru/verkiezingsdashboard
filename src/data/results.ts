import { readFile } from "fs/promises";
import { resolve } from "path";
import { parseStringPromise } from "xml2js";

type ResultData = {
  EML: {
    Result: [{
      Election: [{
        ElectionIdentifier: [ElectionIdentifier];
        Contest: [{
          ContestIdentifier: [{ "$": unknown }];
          Selection: Array<Affiliation | Candidate>;
        }];
      }];
    }];
  };
};

export type ElectionIdentifier = { ElectionName: [string]; ElectionCategory: ["TK"]; };

type BooleanLike = ["yes"] | ["no"];
type Affiliation = { AffiliationIdentifier: [AffliationData]; elected: BooleanLike };
type AffliationData = { "$": [unknown], RegisteredName: [string] }
type Ranking = "1" | "2" | string;
type Candidate = { Candidate: [ CandidateData ]; Ranking: [ Ranking ]; Elected: BooleanLike };
type CandidateData = {
  CandidateIdentifier: [ { "$": { Id: string; ShortCode: string; } } ];
  CandidateFullName: [ { "xnl:PersonName": [CandidateName] } ];
  Gender?: ["male"] | ["female"];
  QualifyingAddress: [{ "xal:Locality": [{ "xal:LocalityName": [string] }] }];
};
type CandidateName = {
  "xnl:NameLine": [{
    _: string;
    "$": { NameType: "Initials" };
  }];
  "xnl:FirstName": [string];
  "xnl:NamePrefix": ["" | "string"];
  "xnl:LastName": [string];
};

export function isCandidateListing(value: Affiliation | Candidate): value is Candidate {
  return Array.isArray((value as Candidate).Candidate);
}

type CandidateList = Record<string, Array<{
  givenName: string;
  namePrefix?: string;
  familyName: string;
  gender?: "male" | "female";
  code: string;
  elected: boolean;
}>>;

export function resultsToCandidateList(results: ResultData): CandidateList {
  let currentParty = "unknown";
  let currentRank = 0;
  const candidateList: CandidateList = {};
  for(const listing of results.EML.Result[0].Election[0].Contest[0].Selection) {
    if (!isCandidateListing(listing)) {
      currentParty = listing.AffiliationIdentifier[0].RegisteredName[0];
      currentRank = 0;
    } else {
      candidateList[currentParty] ??= [];
      const nameData = listing.Candidate[0].CandidateFullName[0]["xnl:PersonName"][0];
      candidateList[currentParty][currentRank] = {
        gender: listing.Candidate[0].Gender?.[0],
        givenName: nameData["xnl:FirstName"][0],
        namePrefix: nameData["xnl:NamePrefix"][0] !== "" ? nameData["xnl:NamePrefix"][0] : undefined,
        familyName: nameData["xnl:LastName"][0],
        code: listing.Candidate[0].CandidateIdentifier[0].$.ShortCode,
        elected: listing.Elected[0] === "yes",
      };
      currentRank++;
    }
  }
  return candidateList;
}

export async function loadResultData(sourcePath = resolve(process.cwd(), "data/tk2021/Resultaat_TK2021.eml.xml")): Promise<ResultData> {
  const resultsSource = await readFile(sourcePath, "utf8");
  const results: ResultData = await parseStringPromise(resultsSource);
  return results;
}

export async function loadResults(sourcePath = resolve(process.cwd(), "data/tk2021/Resultaat_TK2021.eml.xml")): Promise<CandidateList> {
  const resultData = await loadResultData(sourcePath);
  const results = resultsToCandidateList(resultData);
  return results;
}
