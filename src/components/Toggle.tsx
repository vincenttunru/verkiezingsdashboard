import { ChangeEventHandler, FC, useEffect, useState } from "react";

export type Props = {
  onChange: (active:boolean) => void;
  id: string;
  defaultValue?: boolean;
};
export const Toggle: FC<Props> = (props) => {
  const [isActive, setActive] = useState(props.defaultValue ?? false);

  useEffect(() => {
    props.onChange(isActive);
  }, [isActive]);

  const onChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    event.preventDefault();

    setActive(event.target.checked);
  };

  const toggle = () => {
    setActive(oldValue => !oldValue);
  };

  const transitionClasses = "duration-300 ease-in-out";
  const switchContainerActiveClasses = isActive
    ? "bg-green-600 hover:bg-green-400 focus:outline-none focus:ring focus:ring-1 focus:ring-gray-300 focus:ring-offset-2"
    : "bg-gray-300 hover:bg-gray-400 focus:outline-none focus:ring focus:ring-1 focus:ring-green-600 focus:ring-offset-2";
  const switchActiveClasses = isActive
    ? "translate-x-3"
    : "";

  return (
    <>
      <div
        className={`relative w-12 h-8 rounded-full flex-shrink-0 py-1 px-1 cursor-pointer ${switchContainerActiveClasses}`}
        tabIndex={0}
        aria-hidden={true}
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
          toggle();
        }}
        onKeyPress={(event) => {
          if (event.key === " ") {
            event.preventDefault();
            toggle();
          }
        }}
      >
        <div className={`bg-white w-7 h-6 rounded-full shadow-md ${transitionClasses} transform delay-100 ${switchActiveClasses}`}/>
        <input
          className="sr-only"
          type="checkbox"
          id={props.id}
          onChange={onChange}
          checked={isActive}
          role="switch"
        />
      </div>
    </>
  );
};