import { format } from "d3-format";
import { ScaleBand, scaleBand, ScaleLinear, scaleLinear } from "d3-scale";
import { FC, memo, useState } from "react";
import { CandidateData } from "../data/candidates";
import { useMediaQuery } from "../hooks/mediaQuery";

interface Props {
  partyId: string;
  candidates: CandidateData[];
  seats: number;
  absoluteRange?: boolean;
  showAllCandidates?: boolean;
}

type Area = {width: number; height: number};

const unknownColour = "fill-coolGray-300";
const maleColour = "fill-blue-400";
const femaleColour = "fill-rose-400";

export const PartyChart: FC<Props> = memo((props) => {
  const [selectedBar, setSelectedBar] = useState<string | null>(null);
  const isMobile = useMediaQuery("(max-width: 768px)");
  const wrapper: Area = {
    width: 1000,
    height: 600,
  };
  const marginTop = 20;
  const marginRight = 0;
  const marginBottom = 60;
  const marginLeft = 40;

  // TODO: Obtain from data
  const voorkeursdrempel = 17372;

  const maxVotes = Math.max(...props.candidates.map(c => c.votes));
  const desktopCandidateNr = props.showAllCandidates === true ? 100 : 35;
  const maxCandidates = isMobile ? 25 : desktopCandidateNr;

  const maxRangeY = props.absoluteRange === true ? voorkeursdrempel * 2 : maxVotes;

  const boundsScaleX = scaleBand()
    .domain(props.candidates.slice(0, maxCandidates).map(candidate => candidate.id))
    .range([marginLeft, wrapper.width - marginRight]);
  const boundsScaleY = scaleLinear([0, maxRangeY], [wrapper.height - marginBottom, marginTop]);

  const voorkeursdrempelPos = boundsScaleY(voorkeursdrempel);
  const voorkeursdrempelLine = <>
    <line
      style={{
        transform: `translateY(${voorkeursdrempelPos}px)`
      }}
      x1={boundsScaleX(props.candidates[0].id)}
      x2={(boundsScaleX(props.candidates[Math.min(maxCandidates, props.candidates.length) - 1].id) ?? 0) + boundsScaleX.bandwidth()}
      y1={0}
      y2={0}
      className="stroke-warmGray-700 stroke-1 transition-transform duration-500 ease-in-out"
      strokeDasharray="4 4"
      key={props.partyId + "voorkeursdrempel"}
    />
    <text
      x={(boundsScaleX(props.candidates[Math.min(maxCandidates, props.candidates.length) - 1].id) ?? 0) + boundsScaleX.bandwidth()}
      y={0}
      style={{
        transform: `translateY(${voorkeursdrempelPos - 10 }px)`,
        textAnchor: "end",
      }}
      className="text-lg fill-warmGray-700 transition-transform duration-500 ease-in-out"
    >Voorkeursdrempel ({format("0.3s")(voorkeursdrempel)} stemmen)</text>
  </>;

  const seatsPos = boundsScaleX(props.candidates[props.seats].id)!;// - boundsScaleX(props.candidates[props.seats - 1].id)!;
  const seatsLine = (props.seats > Math.min(maxCandidates, props.candidates.length) || props.seats === 0)
    ? null
    : (
      <>
        <line
          x1={0}
          x2={0}
          y1={boundsScaleY(0)}
          y2={boundsScaleY(maxRangeY)}
          strokeDasharray="4 4"
          className="stroke-warmGray-700 stroke-1 transition-transform duration-500 ease-in-out"
          key={props.partyId + "seats"}
          style={{
            transform: `translateX(${seatsPos}px)`
          }}
        />
        <text
          x={0}
          y={boundsScaleY(maxRangeY) - 5}
          className="text-lg fill-warmGray-700 transition-transform duration-500 ease-in-out"
          style={{
            textAnchor: "middle",
            transform: `translateX(${seatsPos}px)`
          }}
        >Behaalde zetels</text>
      </>
    );

  const bars = props.candidates.slice(0, maxCandidates).map(candidate => {
    return (
      <CandidateBar
        key={props.partyId + candidate.id}
        candidate={candidate}
        scaleX={boundsScaleX}
        scaleY={boundsScaleY}
        selected={selectedBar === candidate.id}
        onSelect={() => setSelectedBar(selected => selected === candidate.id ? null : candidate.id)}
      />
    );
  });

  const yAxis = boundsScaleY.ticks(10).map(value => {
    return (
      <g
        key={value}
        style={{ transform: `translateY(${boundsScaleY(value)}px)` }}
      >
        <line x1="0" x2="10" className="stroke-warmGray-700 stroke-1" style={{ transform: "translateX(30px)" }}/>
        <text className="fill-warmGray-800" style={{ transform: "translateY(-3px)" }}>
          {format("0.2s")(value)}
        </text>
      </g>
    );
  });

  const legendDiameter = 30;
  const legendPosition = boundsScaleX.range()[1] - 150;
  const legend = <>
    <g
      style={{ transform: `translateX(${legendPosition}px) translateY(${legendDiameter * 1.5 * 1}px)` }}
    >
      <rect
        width={legendDiameter}
        height={legendDiameter}
        className={`${femaleColour} opacity-70`}
      />
      <text
        className="fill-warmGray-800 text-xl"
        x={legendDiameter * 1.2}
        y={legendDiameter * 0.7}
      >
        Vrouw
      </text>
    </g>
    <g
      style={{ transform: `translateX(${legendPosition}px) translateY(${legendDiameter * 1.5 * 2}px)` }}
    >
      <rect
        width={legendDiameter}
        height={legendDiameter}
        className={`${maleColour} opacity-70`}
      />
      <text
        className="fill-warmGray-800 text-xl"
        x={legendDiameter * 1.2}
        y={legendDiameter * 0.7}
      >
      Man
      </text>
    </g>
    <g
      style={{ transform: `translateX(${legendPosition}px) translateY(${legendDiameter * 1.5 * 3}px)` }}
    >
      <rect
        width={legendDiameter}
        height={legendDiameter}
        className={`${unknownColour} opacity-70`}
      />
      <text
        className="fill-warmGray-800 text-xl"
        x={legendDiameter * 1.2}
        y={legendDiameter * 0.7}
      >
        Onbekend
      </text>
    </g>
  </>;

  const labels = <>
    <text
      x={(boundsScaleX.range()[1] - boundsScaleX.range()[0]) / 2}
      y={boundsScaleY(0) + 50}
      className="text-xl font-bold fill-warmGray-700 transition-transform duration-500 ease-in-out"
      style={{
        textAnchor: "middle",
      }}
    >Kandidaten</text>
  </>;

  const title = `Stemverdeling kandidaten ${props.partyId}`;

  return <>
    <div className="bg-white pt-5 pb-2 md:pt-10 md:pb-5 px-4 md:px-10 rounded" style={{ contain: "content", contentVisibility: "auto" }}>
      <svg
        role="img"
        aria-label={title}
        viewBox={`0 0 ${wrapper.width} ${wrapper.height}`}
      >
        <title>{title}</title>
        {bars}
        {voorkeursdrempelLine}
        {seatsLine}
        {yAxis}
        {legend}
        {labels}
      </svg>
    </div>
  </>;
});

interface BarProps {
  candidate: CandidateData;
  scaleX: ScaleBand<string>;
  scaleY: ScaleLinear<number, number>;
  selected: boolean;
  onSelect: () => void;
}
const CandidateBar: FC<BarProps> = (props) => {
  let colour = unknownColour;
  if (props.candidate.gender === "male") {
    colour = maleColour;
  }
  if (props.candidate.gender === "female") {
    colour = femaleColour;
  }
  const topY = props.scaleY(props.candidate.votes);
  return <g
    className="group cursor-pointer"
    onClick={(event) => {
      event.preventDefault();
      props.onSelect();
    }}
  >
    <rect
      width={props.scaleX.bandwidth()}
      height={1}
      style={{
        transformOrigin: `${props.scaleX(props.candidate.id)}px ${props.scaleY(0)}px`,
        transform: `scaleY(${(props.scaleY.range()[0] - topY) * -1 }) translateX(${props.scaleX(props.candidate.id)}px)`,
      }}
      className={`${colour} ${props.selected ? "opacity-90" : "opacity-70"} group-hover:opacity-100 transition-transform duration-500 ease-in-out`}
      x={0}
      y={props.scaleY(0)}
      data-candidate={props.candidate.id}
    >
      <title>{getName(props.candidate)} ({format("0.3s")(props.candidate.votes)} stemmen)</title>
    </rect>
    <text
      x={props.scaleX(props.candidate.id)}
      y={props.scaleY(0) + 20}
      className={props.selected ? "opacity-100" : "opacity-0 group-hover:opacity-100"}
    >
      {getName(props.candidate)} ({format("0.3s")(props.candidate.votes)} stemmen)
    </text>
  </g>;
};

function getName(candidate: CandidateData): string {
  const nameParts = [
    candidate.givenName,
    candidate.namePrefix,
    candidate.familyName,
  ].filter(isNotNull);
  if (nameParts.length === 0) {
    return candidate.id;
  }
  return nameParts.join(" ");
}

function isNotNull<X>(value: X | null): value is X {
  return value !== null;
}
