import { GetStaticProps } from "next";
import { FC, useState, startTransition } from "react";
import Head from "next/head";
import { PartyChart } from "../components/PartyChart";
import { Toggle } from "../components/Toggle";
import { loadPartyData, Parties } from "../data/candidates";

const Gender: FC<StaticProps> = (props) => {
  const [allVotes, setAllVotes] = useState(false);
  const [allCandidates, setAllCandidates] = useState(false);

  const partyIds = Object.keys(props.partyData);
  const charts = partyIds.map(partyId => {
    // Unfortunately Tailwind doesn't have a scroll-margin-top class,
    // so it's manually adjusted to the headers height of 5rem, defined by h-20,
    // plus the 3.5rem grid gap defined by gap-14:
    return <figure key={partyId} id={encodeURI(partyId)} style={{ scrollMarginTop: "calc(5rem + 3.5rem)" }}>
      <PartyChart
        partyId={partyId}
        candidates={props.partyData[partyId].candidates}
        absoluteRange={!allVotes}
        seats={props.partyData[partyId].seats}
        showAllCandidates={allCandidates}
      />
      <figcaption className="pt-5 pb-5 text-center"><b className="font-bold">{partyId}</b> — {props.partyData[partyId].seats} {props.partyData[partyId].seats === 1 ? "zetel" : "zetels"}</figcaption>
    </figure>;
  });

  const setAllVotesTransition: typeof setAllVotes = (newValue) => {
    startTransition(() => setAllVotes(newValue));
  };
  const setAllCandidatesTransition: typeof setAllCandidates = (newValue) => {
    startTransition(() => setAllCandidates(newValue));
  };

  return <>
    <Head>
      <title>Tweede Kamerverkiezingen 2021</title>
      <meta name="twitter:card" content="summary_large_image"/>
      <meta name="twitter:title" content="Verkiezingsdashboard Tweede Kamerverkiezingen 2021"/>
      <meta name="og:title" content="Verkiezingsdashboard Tweede Kamerverkiezingen 2021"/>
      <meta name="description" content="Welke kandidaten kregen de meeste stemmen in de Tweede Kamerverkiezingen van 2021?"/>
      <meta name="twitter:description" content="Welke kandidaten kregen de meeste stemmen in de Tweede Kamerverkiezingen van 2021?"/>
      <meta name="og:description" content="Welke kandidaten kregen de meeste stemmen in de Tweede Kamerverkiezingen van 2021?"/>
      <meta name="twitter:image" content="https://verkiezingsdashboard.vincenttunru.com/demo.png"/>
      <meta name="twitter:image:alt" content="Stemmen per kandidaat van het CDA: Omtzigt op 342k stemmen, achter Hoekstra's 437k stemmen."/>
      <meta name="og:image" content="https://verkiezingsdashboard.vincenttunru.com/demo.png"/>
      <meta name="twitter:site" content="@VincentTunru"/>
      <meta name="twitter:creator" content="@VincentTunru"/>
    </Head>
    <div className="bg-warmGray-300 pt-20">
      <header className="w-full h-20 px-10 space-x-2 flex items-center fixed top-0 bg-warmGray-700 text-white z-10">
        <h1 className="md:text-xl flex-grow">Tweede Kamerverkiezingen 2021</h1>
        <div className="flex space-x-3 items-center bg-warmGray-500 py-1 md:py-2 px-2 pr-3 md:px-3 md:pr-5 rounded-2xl">
          <Toggle
            id="allVotes"
            onChange={(allVotes) => setAllVotesTransition(allVotes)}
            defaultValue={allVotes}
          />
          <label htmlFor="allVotes" className="text-sm ">
            Alle stemmen tonen
          </label>
        </div>
        <div className="hidden md:flex space-x-3 items-center bg-warmGray-500 py-1 md:py-2 px-2 pr-3 md:px-3 md:pr-5 rounded-2xl">
          <Toggle
            id="allCandidates"
            onChange={(allCandidates) => setAllCandidatesTransition(allCandidates)}
            defaultValue={allCandidates}
          />
          <label htmlFor="allCandidates" className="text-sm ">
            Alle kandidaten tonen
          </label>
        </div>
      </header>
      <section className="p-10 pb-0 lg:flex lg:space-x-14">
        <div className="lg:w-1/2">
          <h2 className="font-bold">Waar kijken we naar?</h2>
          <p className="pb-5">
            Hier zie je per partij hoe de stemmen tijdens de Tweede Kamerverkiezingen van 2021
            waren verdeeld over de kandidaten — één balk per kandidaat.&nbsp;
            <span className="hidden md:inline">
              Zo zie je bijvoorbeeld dat de nummers twee van
              het <PartyLink id="CDA" name="CDA"/>,&nbsp;
              <PartyLink id="Forum voor Democratie" name="FvD"/> en&nbsp;
              <PartyLink id="DENK" name="DENK"/>&nbsp;
              (respectievelijk Pieter Omtzigt, Wybren van Haga en Tunahan Kuzu)
              bijna net zoveel stemmen hebben gekregen als hun lijsttrekkers,
              en welke kandidaten relatief veel voorkeursstemmen binnen hebben gekregen
              (met bijzondere aandacht voor Kauthar Bouchallikht en Lisa Westerveld
              van <PartyLink id="GROENLINKS" name="GroenLinks"/>, en
              Marieke Koekkoek van <PartyLink id="Volt" name="Volt"/>,
              die met voorkeursstemmen een plek in de Kamer hebben bemachtigd,
              en Stephanie Bennett, wederom van GroenLinks, die dat net niet gelukt is).
            </span>
          </p>
        </div>
        <div className="lg:w-1/2">
          <h2 className="font-bold">Waar komt de data vandaan?</h2>
          <p className="pb-5">
            De data is beschikbaar gesteld <a href="https://www.verkiezingsuitslagen.nl/verkiezingen/detail/TK20210317" className="underline hover:text-warmGray-700">
              door de Kiesraad
            </a>, en is te downloaden
            van <a href="https://data.overheid.nl/dataset/verkiezingsuitslag-tweede-kamer-2021" className="underline hover:text-warmGray-700">
              data.overheid.nl
            </a>. Je kunt ook <a href="https://gitlab.com/vincenttunru/verkiezingsdashboard/" className="underline hover:text-warmGray-700">
              de broncode
            </a> van deze website inzien.
          </p>
          <h2 className="font-bold">Wie zit hier achter?</h2>
          <p className="pb-5">
            Hoi, ik ben <a href="https://vincenttunru.com" className="underline hover:text-warmGray-700">
              Vincent
            </a>, aangenaam! Je kunt me ook vinden <a href="https://twitter.com/VincentTunru" className="underline hover:text-warmGray-700">
              op Twitter
            </a> en <a href="https://fosstodon.org/@VincentTunru" className="underline hover:text-warmGray-700">
              op Mastodon
            </a>.
          </p>
        </div>
      </section>
      <main className="grid lg:grid-cols-2 xl2:grid-cols-3 gap-14 py-10 px-2 md:p-10">
        {charts}
      </main>
    </div>
  </>;
};

const PartyLink: FC<{id: string; name: string}> = (props) => {
  return (
    <a
      href={`#${encodeURI(props.id)}`}
      title={`Scroll naar ${props.name}`}
      className="underline hover:text-warmGray-700"
    >{props.name}</a>
  );
}

export default Gender;

type StaticProps = {
  partyData: Parties;
};
export const getStaticProps: GetStaticProps<StaticProps> = async (context) => {
  const partyData = await loadPartyData();
  return {
    props: {
      partyData: partyData,
    },
  };
}
