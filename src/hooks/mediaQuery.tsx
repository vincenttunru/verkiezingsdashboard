import { useEffect, useState } from "react";

export function useMediaQuery(query: string) {
  const [isMatch, setMatch] = useState(false);

  useEffect(
    () => {
      if (typeof window !== "object" || typeof window.matchMedia !== "function") {
        return;
      }

      const listener = (event: MediaQueryList | MediaQueryListEvent) => {
        setMatch(event.matches);
      };

      const mqList = window.matchMedia(query);
      listener(mqList);
      mqList.addEventListener("change", listener);

      return () => {
        mqList.removeEventListener("change", listener);
      };
    },
    [query],
  );

  return isMatch;
}
